#include <stdio.h>
#include <string.h>

#define N 256

struct symbol{
	char symb;
	double probability;
	int before[N];
};

void initializeCode(char *code){
	int i;
	for(i=0;i<N;i++){
		*(code+N*i)='\0';
	}
}

int calculateFrequency(int *frequency, FILE *input){
	int numOfSymbols=0;
	char c;
	while((c=fgetc(input))!=EOF){
		frequency[c]++;
		numOfSymbols++;
	}
	return numOfSymbols;
}

void initializeSymbols(struct symbol *symbolField,int *frequency, int numOfSymbols){
	int i;
	for(i=0;i<N;i++){
		symbolField[i].symb=i;
		if(frequency[i]==0){
			symbolField[i].probability=0;
		} else {
			symbolField[i].probability= (double)frequency[i]/(double)numOfSymbols;
		}
		symbolField[i].before[0]=i;
		symbolField[i].before[1]=-1;
	}
}

void swap(struct symbol *a, struct symbol *b){
	struct symbol pom;
	pom=*a;
	*a=*b;
	*b=pom;
}

void sortByProbability(struct symbol sf[]){
	int i,j, min;
	for(i=0;i<N;i++){
		min=i;
		for(j=i+1;j<N;j++){
			if(sf[j].probability<sf[min].probability){
				min=j;
			}
		}
		swap(&sf[i], &sf[min]);
	}
}

int isOver(struct symbol *sf){
	int i;
	for(i=0;i<N;i++){
		if(sf[i].probability!=-1){
			return 0;
		}
	}
	return 1;
}

int findLowestProb(struct symbol *sf){
	int i;
	for(i=0;i<N;i++){
		if(sf[i].probability!=-1){
			return i;
		}
	}
	return 0;
}


void addBinaryZero(struct symbol *s, char *code){
	int i=0;
	while(s->before[i]!=-1){
		int j=0;
		while(*(code+s->before[i]*N+j)!='\0'){
			j++;
		}
		*(code+s->before[i]*N+j)='0';
		*(code+s->before[i]*N+j+1)='\0';
		i++;
	}
	s->before[i]=-1;
}

void addBinaryOne(struct symbol *s, char *code){
	int i=0;
	while(s->before[i]!=-1){
		int j=0;
		while(*(code+s->before[i]*N+j)!='\0'){
			j++;
		}
		*(code+s->before[i]*N+j)='1';
		*(code+s->before[i]*N+j+1)='\0';
		i++;
	}
	s->before[i]=-1;
}

void addNewBefore(struct symbol *s1, struct symbol *s2){
	int i=0;
	int j=0;
	while(s1->before[i]!=-1){
		i++;
	}
	while(s2->before[j]!=-1){
		s1->before[i]=s2->before[j];
		i++;
		j++;
	}
	s1->before[i]=-1;
	
}

void huffcoder(char *code, struct symbol *sf){
	int p1,p2;
	int i=0;
	while(!isOver(&sf[0])){
		sortByProbability(sf);
		p1=findLowestProb(&sf[0]);
		p2=p1+1;
		if(p2>=N){
			break;
		}
		sf[p2].probability=sf[p1].probability+sf[p2].probability;
		sf[p1].probability=-1;
		
		addBinaryZero(&sf[p1], &code[0]);
		addBinaryOne(&sf[p2], &code[0]);
		
		addNewBefore(&sf[p2], &sf[p1]);		
	}
	
}

void reverseCode(char *code){
	int i,j,k;
	for(i=0;i<N;i++){
		char cHelper[N];
		for(j=0;j<N;j++){
			if(*(code+i*N+j)=='\0')
				break;
		}
		j--;
		k=0;
		for(;j>=0;j--){
			cHelper[k]=*(code+i*N+j);
			k++;
		}
		cHelper[k]='\0';
		k=0;
		while(1){
			*(code+i*N+k)=cHelper[k];
			if(cHelper[k]=='\0'){
				break;
			}
			k++;
		}
	}
}

void writeCodeToFile(char *code, FILE *output){
	int i,j;
	for(i=0;i<N;i++){
		j=0;
		while(*(code+i*N+j)!='\0'){
			fputc(*(code+i*N+j), output);
			j++;
		}
		fputc('\n', output);
	}
}

void compressFile(FILE *in, FILE *out, char *code){
	char read;
	char write=0x00;
	char currentBit;
	int i=7,j;
	while((read=fgetc(in))!=EOF){
		j=0;
		while((currentBit=*(code+read*N+j))!='\0'){
			if(currentBit=='1'){
				write= write | (0x01<<i);
			}
			if(i==0){
				i=7;
				fputc(write, out);
				write=0x00;
			} else {
				i--;
			}
			j++;
		}
	}
	if(write!=0x00){
		fputc(write, out);
	}
}

int main (int argc, char *argv[]){
	FILE *input;
	FILE *codeTable;
	FILE *output;
	input = fopen(argv[1], "rb");
	codeTable = fopen(argv[2], "wb");
	output = fopen(argv[3],"wb");
	
	int i,j;
	int numOfSymbols;
	int frequency[N]  = {0};
	struct symbol symbols[N];
	char code[N][N];
	
	initializeCode(&code[0][0]);
	numOfSymbols=calculateFrequency(&frequency[0], input);
	initializeSymbols(&symbols[0], &frequency[0], numOfSymbols);
	
	
	
	huffcoder(&code[0][0] ,&symbols[0]);
	reverseCode(&code[0][0]);
	writeCodeToFile(&code[0][0], codeTable);
	fseek(input, 0L, SEEK_SET);
	compressFile(input, output, &code[0][0]);	

	

	fclose(input);
	fclose(codeTable);
	fclose(output);
	
	return 0;
}
