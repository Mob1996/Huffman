#include <stdio.h>
#include <string.h>
#define N 256
#define MAXSHIFT 7

void readCodes(FILE *table, char *code){
	char c;
	int i,j;
	for(i=0;i<N;i++){
		j=0;
		while((c=fgetc(table))!='\n'){
			*(code+i*N+j)=c;
			j++;
		}
		*(code+i*N+j)='\0';
	}
}

int contains(char *code, char *table){
	int i,j;
	for(i=0;i<N;i++){
		j=0;
		while(*(table+i*N+j)==*(code+j)){
			if(*(table+i*N+j)=='\0'){
				
				return i;
			}
			j++;
		}
	}
	return -1;
}

void decode(FILE *in, FILE *out, char *codeTable){
	char write, mask, bit;
	int read;
	char currentCode[N];
	int shift, j=0;
	while(1){
		if((read=fgetc(in))==EOF){
			break;
			
		}
		shift=MAXSHIFT;
		while(shift>=0){
			mask=0x01;
			mask=mask<<shift;
			shift--;
			bit=read&mask;
			if(bit==0x00){
				currentCode[j]='0';
			} else {
				currentCode[j]='1';
			}
			j++;
			currentCode[j]='\0';
			if((write=contains(&currentCode[0], codeTable))!=-1){
				j=0;
				currentCode[j]='\0';
				fputc(write, out);
			}
		}
	}
}

int main (int argc, char *argv[]){
	FILE *codeTable;
	FILE *input;
	FILE *output;
	if((codeTable = fopen(argv[1], "r"))==NULL){
		printf("Neuspjelo otvaranje %s", argv[1]);
		return -1;
	}
	if((input = fopen(argv[2], "rb"))==NULL){
		printf("Neuspjelo otvaranje %s", argv[2]);
		return -1;
	}
	if((output=fopen(argv[3], "wb"))==NULL){
		printf("Neuspjelo otvaranje %s", argv[3]);
		return -1;
	}
	

	char code[N][N];
	int i,j;
	
	
	
	readCodes(codeTable, &code[0][0]);
	decode(input,output, &code[0][0]);
	
	
	fclose(codeTable);
	fclose(input);
	fclose(output);
	return 0;
}
